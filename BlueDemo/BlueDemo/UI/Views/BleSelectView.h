//
//  BleSelectView.h
//  CoolFlow
//
//  Created by 刘红 on 16/3/28.
//  Copyright © 2016年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BleSelectView : UIView


+ (BleSelectView *)shareInstance;

- (void)show;
- (void)dismiss;

@end
